# create a directory with names and phone extenetions
phone = {"George": 5532, "Emmanuelle":282, "Lin": 440, "James": 990}

# and more entries
phone["Richardpopulous"] = 2919
phone["Monica"] = 221

# if name is longer than nine characters, print the first 6 followed by '...'
def phone_directory():
    for name, num in phone.items():
        if len(name) > 9:
            print name[0:6] + '...  ==> {1:10}'.format(name, num)
            continue
        print "{0:10} ==> {1:10}".format(name, num)

print phone_directory()